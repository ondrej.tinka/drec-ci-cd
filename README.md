# DREC 

Data recorder pro rozličná data z rozličných kryptoburz.<br>

V současnosti podporujeme:
- Binance
- Kraken
- Coinbase

## Jak to funguje (nyní)?
Drec za pomoci knihovny [cryptofeed](https://github.com/bmoscon/cryptofeed) stahuje online data, které pak ukládá do databáze [MongoDB](https://www.mongodb.com). V současnosti je možné stahovat data pouze z jedné burzy najednou, nicméne data chanelů a obchodovaných párů je možno najednou stahovat více. Ke konfiruaci slouží soubor `default.conf`, k inspiraci může posloužit soubor `default.conf.exmaple` k dispozici v tomto repozitáři.<br>

Dockerfile a .gitlab-ci.yml slouží pro Gitlab CI/CD build.

## Jak to bude fungovat?
Plán je vytvořit strukturu, která automaticky vybuildí a nasadí aplikaci do nějakého Kubernetees clusteru. V rámci prozkoumávání vod AWS je naplánu vybuilděný image nejdříve nahrát do ECR ([Elastic Container Registry](https://aws.amazon.com/ecr/)) a poté provozovat v EKS ([Elastic Kubernetes Service](https://aws.amazon.com/eks)) se standalone databází. Celá infratruktura pak bude vytvořena a spravována kombinací Ansiblu a Terraformu.<br>

Nicméně pro nějaké praktické využití bude asi ekonomičtější pak celou věc přesunout na DigitalOcean.<br>

Pokus o ascii art: <br>

`|________|            |______________|           |_____|      |_____|      |_________|`<br>
`| gitlab |  -- CI --> | Build server | -- CD --> | ECR | ---- | EKS | ---> | MongoDB |`<br> 
`|________|            |______________|           |_____|      |_____|      |_________|`

## Komponenty aplikace
Aplikace se skládá ze tří komponent:
1. Hlavní kód aplikace - `drec.py`
2. Kód pro nahrávání konfigurace - `loadConf.py` (Bude přejmenováno)
3. Kód pro překlad konfigurace na objekty knihovny cryptofeed - `crossfireT`
<br>
Hlavní kód byl přebrán z [dema](https://github.com/bmoscon/cryptofeed/blob/f1908dee64d639b6d76afee6a70599226241f638/examples/demo_mongo.py) knihovny cryptofeed a následně parametrizován. 

## Jak to zprovoznit?
##### Prerekvizity:
1. Python 3.8 + virtualenv (ideálně)
2. MongoDB na defaultním portu
3. Nějaký pěkný aplikační adresář

##### Samotná "instalace":
1. Naklonovat repozitář
2. Z adresáře drec udělat virtualenv
3. Nainstalovat dependecies v requirements.txt

##### Spuštění aplikace:
1. Vytvoření souboru `default.conf` s konfigurací (aplikace konfiguraci při spuštění validuje) 
2. Spuštění `drec.py` (Přes `python drec.py` - skript neobsahuje shebang).
3. Pro zastavení aplikace zmáčknout `CTRL+C` (nebo to prostě zabít při běhu na pozadí)

Na data se lze pak podívat následujícím způsobem:
1. V novém terminálovém okně se přihlásíme do databáze - příkaz `mongo`
2. Přejdeme do databáze dle názvu burzy (v případě nejistoty stačí pustit `show databases`)
3. Po zadání příkazu `collection.find()` se zobrazí část dat - collection v uvedném příkazu je název kolekce z které chceme zobrazit data, kolekce jsou pojmenovány po data chanelech (v případě nejistoty stačí spusti `show collections`)
    1. Popřípadě lze pak pomocí `key = value` filtrovat konkrétnější data, například při použití `collection.find({ "symbols" = "BTC-USD"})` se zobrazí pouze data týkající se páru BTC - USD.


## Pár slov na závěr a k čemu to vlastně je

Tento projekt vzniká ve spolupráci s Georgy Ponimatkinem ([github zde](http://github.com/ponimatkin)), který zde figuruje spíš jako "zákazník" nebo buisness analytyk a určuje co by to mělo umět. Samotný kód je pak z drtivé většiny můj (kromě převzaté části). Na nasbíraných datech pak Georgy dělá nějakou analýzu a vyrábí pěkné grafy. Cílem projektu je vyrobit bota pro poskytování likvidity. <br>

Tento repozitář je klon privátního repozitáře z githubu, primární účel pro tento repozitář je vývoj Gitlab CI/CD pipeliny a celého toho systému popsaného výše. Commit history z původního repozitáře, který vlasní Georgy (a tudíž ho nemůžu zveřejnit - nemám práva), můžu pak ukázat na nějakém videohovoru. 