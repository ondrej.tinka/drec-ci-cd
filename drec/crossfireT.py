from cryptofeed.backends.mongo import BookDeltaMongo, BookMongo, TradeMongo, TickerMongo
from cryptofeed.defines import BOOK_DELTA, L2_BOOK, L3_BOOK, TRADES, TICKER, VOLUME, FUNDING
import cryptofeed.exchanges as exchanges

def conf2obj(config_item, crossfire_dict):
    if crossfire_dict.get(config_item):
        return crossfire_dict.get(config_item)
    elif crossfire_dict == DATA_CHANELS and config_item == "":
        return False
    else:
        print(f"{config_item} not in crossfire table")
        return False
    
def get_obj_list(conf_list, crossfire_dict):
    obj_list = list()
    for conf_item in conf_list:
        _to_append = conf2obj(conf_item, crossfire_dict)
        if _to_append:
            obj_list.append(_to_append)
        else:
            continue

    return obj_list

def get_callbacks_dict(callback_list, db_name, collection_dict):
    callback_dict = dict()

    for callback in callback_list:
        if collection_dict.get(callback):
            callback_dict.update({ conf2obj(callback, CALLBACK_CHANELS) : conf2obj(callback, DB_CALLBACKS)(db_name, collection=collection_dict.get(callback)) })
        else:
            print(f"{callback} not in collections dictionary.")
            continue
    
    return callback_dict



DATA_CHANELS = {"TRADES" : TRADES,
                "TICKER" : TICKER,
                "VOLUME" : VOLUME,
                "FUNDING" : FUNDING,
                "L2_BOOK" : L2_BOOK,
                "L3_BOOK" : L3_BOOK}

CALLBACK_CHANELS = {"TRADES" : TRADES,
                "TICKER" : TICKER,
                "VOLUME" : VOLUME,
                "FUNDING" : FUNDING,
                "BOOK_DELTA" : BOOK_DELTA,
                "L2_BOOK" : L2_BOOK,
                "L3_BOOK" : L3_BOOK}

EXCHANGES = {"Coinbase" : exchanges.Coinbase,
             "Kraken" : exchanges.Kraken, 
             "Binance": exchanges.Binance}

DB_CALLBACKS = {"TRADES" : TradeMongo,
                "TICKER" : TickerMongo,
                "BOOK_DELTA" : BookDeltaMongo,
                "L2_BOOK" : BookMongo,
                "L3_BOOK" : BookMongo}

'''
_DB_CALLBACKS = {"TRADES" : {TRADES: TradeMongo('coinbase', collection='trades')},
                "TICKER" : {TICKER: TickerMongo('coinbase', collection='ticker')},
                "BOOK_DELTA" : {BookDeltaMongo('coinbase', collection='l2_book')},
                "L2_BOOK" : {L2_BOOK: BookMongo('coinbase', collection='l2_book')},
                "L3_BOOK" : {L3_BOOK: BookMongo('coinbase', collection='l3_book')}}
'''