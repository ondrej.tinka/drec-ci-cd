from cryptofeed import FeedHandler

from loadConf import load_config, str2list, validate_config
from crossfireT import DATA_CHANELS, EXCHANGES, DB_CALLBACKS, conf2obj, get_obj_list, get_callbacks_dict

from sys import exit


def main(exchange, max_depth, data_chanels, pairs, db_name, collection_dict):
    """
    Because periods cannot be in keys in documents in mongo, the bids and asks dictionaries
    are converted to BSON. They will need to be decoded after being read
    """
    f = FeedHandler()
    f.add_feed(exchange(max_depth=max_depth, channels=data_chanels,symbols=pairs, callbacks=get_callbacks_dict(str2list(load_config("data"), " "), db_name, collection_dict)  ))
    f.run()


if __name__ == '__main__':
    
    if not validate_config("max_depth", "data", "pairs", "exchange", "db_name"):
        print("Some key config items are missing. EXITING")
        exit(1)

    db_name = load_config("db_name")
    collection_dict = {"TRADES" : "trades",
                "TICKER" : "ticker",
                "VOLUME" : "volume",
                "FUNDING" : "funding",
                "BOOK_DELTA" : "l2_book",
                "L2_BOOK" : "l2_book",
                "L3_BOOK" : "l3_book"}

    max_depth = load_config("max_depth", pre=int)
    data_chanels = get_obj_list(str2list(load_config("data"), " "), DATA_CHANELS)
    pairs = str2list(load_config("pairs"), " ")
    exchange =  conf2obj(load_config("exchange"),  EXCHANGES)

    main(exchange, max_depth, data_chanels, pairs, db_name, collection_dict)
